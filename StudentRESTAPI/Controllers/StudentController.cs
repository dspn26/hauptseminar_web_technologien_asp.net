using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using StudentRESTAPI.Models;
using StudentRESTAPI.Services;

namespace StudentRESTAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StudentController : ControllerBase
    {

        public StudentController()
        {
        }

        // Return all students if a Get-Request for this route takes place
        // The return type will be a JSON object
        [HttpGet]
        public ActionResult<List<Student>> GetAll() => StudentService.GetAll();

        // Get a single Student
        // Return type will again be JSON-Data
        [HttpGet("{id}")]
        public ActionResult<Student> Get(int id)
        {
            var student = StudentService.Get(id);
            if(student is null)
                return NotFound();
            return student;
        }

        [HttpPost]
        public IActionResult Create(Student student)
        {
            StudentService.AddStudent(student);
            return CreatedAtAction(nameof(Create), new { id = student.Id}, student);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, Student student)
        {
            if(id != student.Id)
                return BadRequest();
            var existingStudent = StudentService.Get(id);
            if(existingStudent is null)
                return NotFound();
            StudentService.Update(student);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var student = StudentService.Get(id);
            if(student is null)
                return NotFound();
            StudentService.Delete(id);
            return NoContent();
        }

    }
}