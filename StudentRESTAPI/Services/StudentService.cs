using StudentRESTAPI.Models;
using System.Collections.Generic;
using System.Linq;

namespace StudentRESTAPI.Services
{
    public static class StudentService
    {
        static List<Student> Students { get; }
        static int nextId = 3;

        static StudentService()
        {
            Students = new List<Student>
            {
                new Student{Id = 1, Name = "Max Mustermann", Subject = "Informatik"},
                new Student{Id = 2, Name = "Bettina Meyer", Subject = "Psychologie"}
            };
        }

        public static List<Student> GetAll() => Students;
        public static Student Get(int id) => Students.FirstOrDefault(s => s.Id == id);

        public static void AddStudent(Student student)
        {
            student.Id = nextId++;
            Students.Add(student);
        }

        public static void Delete(int id)
        {
            var student = Get(id);
            if(student is null)
                return;
            Students.Remove(student);
        }

        public static void Update(Student student)
        {
            var index = Students.FindIndex(s => s.Id == student.Id);
            if(index == -1)
                return;
            Students[index] = student;
        }
    }
}