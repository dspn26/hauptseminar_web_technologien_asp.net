using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using BlogIt.Models;

    public class BlogItContext : IdentityDbContext<IdentityUser, IdentityRole, string>
    {
        public BlogItContext (DbContextOptions<BlogItContext> options)
            : base(options)
        {
        }

        public DbSet<BlogIt.Models.Article> Article { get; set; }
    }
