using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BlogIt.Models;
using Microsoft.AspNetCore.Authorization;
using BlogIt.Helper;

namespace BlogIt.Pages_Articles
{
    [Authorize]
    public class DeleteModel : PageModel
    {
        private readonly BlogItContext _context;
        private readonly UserHelper _userHelper;

        public DeleteModel(BlogItContext context, UserHelper userHelper)
        {
            _context = context;
            _userHelper = userHelper;
        }

        [BindProperty]
        public Article Article { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Article = await _context.Article.Include(article => article.Author).FirstOrDefaultAsync(m => m.ID == id);

            if (Article == null)
            {
                return NotFound();
            }
            if(_userHelper.CurrentUserIsAuthor(Article.Author.Id))
            {
                return Page();
            }
            else
            {
                return NotFound();
            }
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Article = await _context.Article.FindAsync(id);

            if (Article != null)
            {
                _context.Article.Remove(Article);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
