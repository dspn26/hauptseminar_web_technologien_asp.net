using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BlogIt.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace BlogIt.Pages_Articles
{
    public class IndexModel : PageModel
    {
        private readonly BlogItContext _context;

        public IndexModel(BlogItContext context)
        {
            _context = context;
        }

        public IList<Article> Article { get;set; }

        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }

        public async Task OnGetAsync()
        {
            var articles = from a in _context.Article.Include(article => article.Author).AsNoTracking()
                            select a;
            if(!string.IsNullOrEmpty(SearchString))
            {
                articles = articles.Where(s => s.Heading.Contains(SearchString));
            }
            Article = await articles.ToListAsync();
        }
    }
}
