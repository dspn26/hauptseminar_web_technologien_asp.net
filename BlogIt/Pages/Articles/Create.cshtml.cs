using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using BlogIt.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

namespace BlogIt.Pages_Articles
{

    [Authorize]
    public class CreateModel : PageModel
    {
        private readonly BlogItContext _context;
        private readonly UserManager<IdentityUser> _userManager;
                
        public CreateModel(BlogItContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [BindProperty]
        public Article Article { get; set; }

        public IActionResult OnGet()
        {
            return Page();
        }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            IdentityUser blogItUser = await _userManager.GetUserAsync(User);
            Article.PublishingDate = DateTime.Now;
            Article.Author = blogItUser;
            ModelState.ClearValidationState(nameof(Article));
            if (!TryValidateModel(Article, nameof(Article)))
            {
                return Page();
            }
            _context.Article.Add(Article);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
