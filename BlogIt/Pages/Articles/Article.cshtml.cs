using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BlogIt.Models;

namespace BlogIt.Pages_Articles
{
    public class ArticleModel : PageModel
    {
        private readonly BlogItContext _context;

        public ArticleModel(BlogItContext context)
        {
            _context = context;
        }

        public Article Article { get; set; }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            Article = await _context.Article.Include(article => article.Author).FirstOrDefaultAsync(m => m.ID == id);

            if (Article == null)
            {
                return NotFound();
            }
            ViewData["ArticleName"] = Article.Heading;
            return Page();
        }
    }
}
