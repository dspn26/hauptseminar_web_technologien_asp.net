using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BlogIt.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Identity;
using BlogIt.Helper;

namespace BlogIt.Pages_Articles
{
    [Authorize]
    public class EditModel : PageModel
    {
        private readonly BlogItContext _context;
        private readonly UserHelper _userHelper;
        
        public EditModel(BlogItContext context, UserHelper userHelper)
        {
            _context = context;
            _userHelper = userHelper;
        }

        [BindProperty]
        public Article Article { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Article = await _context.Article.Include(article => article.Author).AsNoTracking().FirstOrDefaultAsync(m => m.ID == id);
            if (Article == null)
            {
                return NotFound();
            }
            if(_userHelper.CurrentUserIsAuthor(Article.Author.Id))
            {
                return Page();
            }
            else
            {
                return NotFound();
            }
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(int? id)
        {
            var articleToUpdate = await _context.Article.Include(article => article.Author).FirstOrDefaultAsync(m => m.ID == id);
            // Needed to complete model validation, but won't be updated because of using TryUpdateModel
            Article.Author = articleToUpdate.Author;
            Article.ID = articleToUpdate.ID;
            Article.PublishingDate = articleToUpdate.PublishingDate;
            
            ModelState.ClearValidationState(nameof(Article));
            if(!TryValidateModel(Article, nameof(Article)))
            {
                return Page();
            }
            if(articleToUpdate == null)
            {
                return NotFound();
            }
            if(await TryUpdateModelAsync<Article>(
                articleToUpdate,
                "article",
                a => a.Heading, a => a.ShortDescription, a => a.Text))
            {
                // _context.Entry(Article).State = EntityState.Modified;
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ArticleExists(Article.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToPage("./Index");
            }
            return Page();
        }

        private bool ArticleExists(int id)
        {
            return _context.Article.Any(e => e.ID == id);
        }
    }
}
