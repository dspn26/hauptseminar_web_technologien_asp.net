using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace BlogIt.Models
{
    public class Article
    {
        public int ID { get; set; }

        [DataType(DataType.Date)]
        public DateTime PublishingDate { get; set; }
        public IdentityUser Author { get; set; }

        public string Heading { get; set; }
        public string ShortDescription { get; set; }

        [DataType(DataType.MultilineText)]
        public string Text { get; set; }
    }
}