using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace BlogIt.Helper
{
    public class UserHelper
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserHelper(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public bool CurrentUserIsAuthor(string articleAuthorId)
        {
             string currentUserId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
             return (currentUserId != null && currentUserId == articleAuthorId);
        }
    }
}