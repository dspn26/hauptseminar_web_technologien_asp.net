using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BlogIt.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace BlogIt.Areas.Identity.Pages.Account
{
    [Authorize]
    public class ProfileModel : PageModel
    {
        private readonly BlogItContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public ProfileModel(BlogItContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IList<Article> OwnArticles { get; set; }
        
        public async Task<IActionResult> OnGetAsync()
        {

            var currentUser = await _userManager.GetUserAsync(User);
            if(currentUser == null)
            {
                return NotFound();
            }
            var articles = from a in _context.Article.Include(article => article.Author).AsNoTracking()
                            where a.Author.Id == currentUser.Id
                            select a;
            OwnArticles = await articles.ToListAsync();
            return Page();
        }
    }
}
